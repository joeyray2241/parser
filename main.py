import db
import parser

url = 'https://habr.com/ru/hub/python/page'
host_name = 'localhost'
user = 'root'
password = 'root'
db_name = 'data_db'

data = parser.parser(url)
connection = db.create_connection(host_name, user, password, db_name)

if type(data) == list:
    for elem in data:
       check = db.check_query(db_name, connection, **elem)

       if check:
           db.create_query(connection, **elem)

