from bs4 import BeautifulSoup as bs
import requests as rq
import json

#TODO: рефакторить функцию - вынести переменные 
def parser(url):
    htmls = get_page(url)

    links = get_links(htmls)

    return links

def get_page(url):
    page = 1

    arr = []

    while page < 5:
        new_url = url + str(page)
        req = rq.get(new_url)
        html = bs(req.content, 'html.parser')

        page = page + 1
        arr.append(html)

    return arr


def get_links(arr):
    links = []

    for html in arr:
        for hub in html.select(".tm-article-snippet__title-link"):
            obj = {
            "href": hub["href"],
            "title": hub.text
            }
            links.append(obj)

    return links