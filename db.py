from operator import truediv
import mysql.connector

def create_connection(host_name, user_name, user_pass, db_name):
    connection = mysql.connector.connect(
        host = host_name,
        user = user_name,
        passwd = user_pass,
        database = db_name
    )

    if connection:
        return connection
    else:
        return -1

def create_table(connection, query):
    return execute_query()

def execute_query(connection, query):
    cursor = connection.cursor()
    flag = True
    
    try:
        cursor.execute(query)
        connection.commit()
    except:
        flag = False

    return flag

def check_query(db_name, connection, href, title):
    query = 'select * from ' + db_name + ' where link="' + href + '"'
    return execute_query(connection, query)

def create_query(connection, href, title):
    query = 'insert into articles (link, title) values("' + href + '", "' + title + '");'
    execute_query(connection, query)